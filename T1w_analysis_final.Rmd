---
title: "T1w analysis mask smooth"
author: "Marie"
output: 
  html_document:
        toc: TRUE
        toc_depth: 4
        number_sections: FALSE
        toc_float: TRUE
---


**General Info and Structure:**

The data to replicate this Markdown can be downloaded from https://osf.io/vjyan/ and should be put in a folder named "data" in the directory of the R-Project-File.
In this Markdown, Linear Mixed Models (LMMs) are used in analogy to those replicating the results from the VBM-Analysis in SPM, but with quantitative T1-values as the outcome variable. The extracted values from the clusters identified by the VBM-analysis and the TFCE-Method are used as ROIs here. The Mp2RAGE-Sequence produces a quantitative T1map and a T1-weighted(UNI)-image, which is the basis for the VBM-analysis. Here, the "raw", unpreprocessed T1-weighted intensity values were extracted using fslmaths and fslstats (mean within ROIs).

Initially, the impact of the factor group (TSST or Placebo-TSST) is investigated across all clusters (GLOBAL MODEL), followed by an investigation for each cluster separately (LOCAL MODELS).

# Linear Mixed Models Analysis of UNI values in VBM-clusters


Install required packages
```{r packages, echo=F, message=FALSE, warning=F, tidy=T}
require(pacman)
pacman::p_load("plyr", "tidyr", "lattice", "ggplot2", "dplyr", "patchwork", "afex", "emmeans", "ggpol", "psychReport", "knitr", "lme4", "GGally","reshape2","reshape","car","lattice",
          "formatR",    "cowplot","readr", "rmarkdown","Rmisc","devtools","gghalves","plotrix", "here")

source("data/diagnostic_fcns.r")
options(scipen=999)


```



```{r load preprocessed data from wsp,echo=F, warning=F,message=F, tidy=T}

load("clean_data_plasticitypaper.RData")

```


set plot aesthetics

```{r plot aesthetics, echo=F, warning=F, message=F, tidy=T}
# set some aesthetics
# width and height variables for saved plots
w = 6
h = 4
# nudge variables for rainclouds
nudge_dot_a = -.16
nudge_dot_b = .16
nudge_cloud_a = -.26
nudge_cloud_b = .26
pd <- position_dodge(width = 0.05)
# colors
color_1 = "grey26"#"#00436A"
color_2 = "orangered2"#"#FD6161"
# Sizes
line_summary_size = 1.5
```
  


## load & prepare data



## read in and prepare T1-data


### normalize T1 to MNI in analogy to UNI

UNI-images' warping to MNI space was estimated and the deformation maps were applied to T1-maps. This was done because T1 and Uni were acquired with the same sequence (MP2RAGE) amd are thus overlapping, but the UNI-contrast is better working in the classification algorithm of SPM.

```{asis normalize, eval=F}
%%%%%% PREPROCESSING SCRIPT %%%%%%

%% Set up basic parameters
clear all


%close all

substring={}


a= [1:67]


for partnum= a;


        substring{end+1} = sprintf('NECOS0%02d%s',partnum,'_post');
        substring{end+1} = sprintf('NECOS0%02d%s',partnum,'_pre');

end


T1_dir=('/..../T1_analysis/T1_denoised/')
%T1_dir=('/..../T1_analysis/R1_denoised/')
%anat_dir=('/data/pt_nro132_marie/from_eger1/muhlig/owncloud/MRdata/anatomical/UNI_denoised/brainmasked/')
anat_dir=('/..../UNI_denoised/')

 
for imnum=1:134;

pname=substring(imnum);
pname=cell2mat(pname);

pname



coregister=0;
segment=0;
normalisation=1;
Smooth=0;

%% Normalization

if normalisation

%%%%%%%%own normalization%%%%%%

%%%%%%%%disable normalise.est.-part if y_UNI_denoised are in '/..../UNI_denoised/already %%%%%%%%%%%%%%%%%%%%%%%%

% %anaim=cellstr(spm_select('FPList', anat_dir, (sprintf('%s_UNI_denoised_m.nii', pname))));
anaim=cellstr(spm_select('FPList', anat_dir, (sprintf('%s_UNI_denoised.nii', pname))));
% 
% matlabbatch{1}.spm.spatial.normalise.est.subj.vol = anaim;
% matlabbatch{1}.spm.spatial.normalise.est.eoptions.biasreg = 0.0001;
% matlabbatch{1}.spm.spatial.normalise.est.eoptions.biasfwhm = 60;
% matlabbatch{1}.spm.spatial.normalise.est.eoptions.tpm = {'/data/pt_nro132_marie/from_eger1/spm-muhlig/tpm/TPM.nii'};
% matlabbatch{1}.spm.spatial.normalise.est.eoptions.affreg = 'mni';
% matlabbatch{1}.spm.spatial.normalise.est.eoptions.reg = [0 0.001 0.5 0.05 0.2];
% matlabbatch{1}.spm.spatial.normalise.est.eoptions.fwhm = 0;
% matlabbatch{1}.spm.spatial.normalise.est.eoptions.samp = 3;
% 
%     
%     spm_jobman('run', matlabbatch);
%     clear matlabbatch;
    
defim=cellstr(spm_select('FPList', anat_dir, (sprintf('y_%s_UNI_denoised.nii', pname))));

matlabbatch{1}.spm.spatial.normalise.write.subj.def = defim;
matlabbatch{1}.spm.spatial.normalise.write.subj.resample = anaim;
%matlabbatch{1}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
%                                                          78 76 85];
matlabbatch{1}.spm.spatial.normalise.write.woptions.bb = [-90 -126 -72
                                                          90 90 108];
matlabbatch{1}.spm.spatial.normalise.write.woptions.vox = [1.5 1.5 1.5];
matlabbatch{1}.spm.spatial.normalise.write.woptions.interp = 4;
matlabbatch{1}.spm.spatial.normalise.write.woptions.prefix = 'w';

    spm_jobman('run', matlabbatch);
    clear matlabbatch;
    
end


 
end %end a participant loop

```



```{asis brainmask}
#!bin/bash

###Marie Uhlig 11.11.2021

#mask uni-images with GM-mask (VBM at thresh0.1)


##load FSL environment

T1DIR=/..../T1_analysis/


for brainfile in /..../UNI_denoised/wNECOS*_UNI_denoised.nii ##this selects all warped pre&post-T1-images

do 

filename=${brainfile##*/}


echo $filename


fslmaths $brainfile -mul /..../CAT12_6_r1450_segment_longitudinal/statistics/one-sample-ttest-mask-thresh01/mask.nii /..../UNI_denoised/"GM_"$filename


#fslmaths $brainfile -mul /..../CAT12_6_r1450_segment_longitudinal/statistics/one-sample-ttest-WMmask-thresh01/mask.nii /..../UNI_denoised/"WM_"$filename


done

#fslstats /..../CAT12_6_r1450_segment_longitudinal/mri/smwp1rNECOS067_delta_UNI_denoised.nii -k /..../CAT12_6_r1450_segment_longitudinal/statistics/two-sample-ttest-deltaimgs_avgTIV_avgmFD_no36484966/two-sample-ttest-deltaimgs_avgTIV_avgmFD_no36484966_TFCEsmith_threshFWE05_cluster1_ACC.nii -M

```


### smooth warped T1-Images

The warped T1-images were smoothed with an 8mm Gaussian Kernel in analogy to the preprocessed T1-weighted images.

```{asis smooth, eval=F}
%-----------------------------------------------------------------------
% Job saved on 17-Feb-2020 19:50:39 by cfg_util (rev $Rev: 7345 $)
%%%%edited: 08.04.2020 Marie Uhlig
% spm SPM - SPM12 (7487)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
%%
matlabbatch{1}.spm.spatial.smooth.data = {
                                          '/..../UNI_denoised/GM_wNECOS001_post_UNI_denoised.nii,1'
                                          
.
.
.
'/..../UNI_denoised/GM_wNECOS067_pre_UNI_denoised.nii,1'
                                          };
%%
matlabbatch{1}.spm.spatial.smooth.fwhm = [8 8 8];
matlabbatch{1}.spm.spatial.smooth.dtype = 0;
matlabbatch{1}.spm.spatial.smooth.im = 0;
matlabbatch{1}.spm.spatial.smooth.prefix = 's';

    spm_jobman('run', matlabbatch);
    clear matlabbatch;



```



### extract UNI-values using FSL

Average UNI-intensity values were extracted from within the significant VBM-Clusters.

```{bash extract, eval=F}
#!bin/bash

###Marie Uhlig 08.04.2020

##extract mean (non-zero)-value from within ROi mask from VBM analysis from warped, smoothed, UNI-images

##load FSL environment



for maskfile in /..../CAT12_6_r1450_segment_longitudinal/statistics/ROIs/VBMcluster/two-sample-ttest-deltaimgs_avgTIV_avgmFD_no36484966_TFCEsmith_threshFWE05_cluster*.nii #selects all ROI masks created from VBM-analysis



do 

echo $maskfile

maskname=${maskfile##*/}

echo ${maskname%%.nii}



for brainfile in /..../UNI_denoised/sGM_w*_UNI_denoised.nii ##this selects all smoothed & warped pre&post-UNI-images

do 

filename=${brainfile##*/}


echo $filename
echo ${filename%%.nii} >> /..../UNI_denoised/"ROI_extracted_wUNIvalues"${maskname%%.nii}".txt"



fslstats $brainfile -k $maskfile -M #uses maskfile to mask brainfile and then gives the mean of all non-zero voxels within this ROI
ROIval=$( fslstats $brainfile -k $maskfile -M )

echo $ROIval >> /..../UNI_denoised/"ROI_extracted_wUNIvalues"${maskname%%.nii}".txt"



done

done



```


### read in extracted UNI-data

This is the data that was extracted from the warped and smoothed UNI-images. Masks created from the VBM-results were used as ROIs and the data was extracted from the smoothed & warped UNI-images using fslmaths. The Numbers represent the average intensity values within these clusters.

Regions were named according to the 
be renamed according to the dartel_neuromorphometrics atlas in SPM. 

```{r regions, warning=F, message=F, tidy=T}

twROI.df.l<- read.csv("data/UNI_extractedT1wvalues_long.csv")

twROI.df.l<- twROI.df.l%>%
  mutate(ID=as.factor(ID),
         stress=as.factor(stress),
         variable=as.factor(variable),
         c.name=as.factor(variable.new),
         scan=as.factor(scan),
         value=as.numeric(value))



twROI.df.l$scan <- relevel(twROI.df.l$scan,ref = "pre")


```


```{r MODEL-SETUP,warning=F,tidy=T}

#set up model and check distributions

#data
model.df <- join(twROI.df.l,combi.cov.w,by=c("ID","stress","scan"))

#remove NAs
model.df <- model.df[complete.cases(model.df$value),]
model.df <- model.df[complete.cases(model.df$variable),]




model.df <-
  model.df %>%
  dplyr::select(

   ID,
   stress,
   scan,
   value,
   variable=c.name,
   TIV,
   MFD
 )


#scaling of UNI-values to identify strong outliers


z.value <- scale(model.df$value)
model.df$ID[z.value< (-3) | z.value>3]

model.df$value[z.value< (-3) | z.value>3] <- NA

model.df$value[model.df$ID=="NECOS057"] <- NA


#NECOS 57 was identified as an outlier  is therefore excluded (like he was in the T1-analysis)

#data distribution & transformation

hist(model.df$TIV)
#good
hist(model.df$MFD)
#slightly skewed
hist(log(model.df$MFD))
#better-> log-transform MFD
model.df$log.MFD <- log(model.df$MFD)

hist(model.df$value)
#fine


#scale covariates
model.df$zTIV <- scale(model.df$TIV)
model.df$zlog.MFD <- scale(model.df$log.MFD)

```



## GLOBAL LMM extracted UNI: Test interaction of group:time:cluster


A global model including all significant clusters from the VBM-Analysis will be set up. Cluster ID will be included as as predictor. This model should test the effect of the group*time interaction on UNI-intensity-values using mixed models.

The full model includes a 3-fold interaction between group, time and cluster. A significant effect would mean a different direction or steepness of the interaction effect of group and time between the clusters. 
This is compared to a NULL-Model lacking the interaction effects (only main effects of time, group and variable are included).
TIV and mFD are included as covariates of no interest and a random intercept is modeled per subject.  



```{r MODEL-TEST, warning=F, message=F, tidy=T}
##model setup
res <- lmer(value~stress*scan*variable+zlog.MFD+zTIV+(1|ID),data=model.df[complete.cases(model.df),],REML=F)

vif(lm(value~scan+stress+variable,model.df))

null <- lmer(value~stress+scan+variable+zlog.MFD+zTIV+(1|ID),model.df[complete.cases(model.df),],REML=F)

anova(null,res, test="Chisq")

#diagnostics
diagnostics.plot(res)#much better when transformed
ranef.diagn.plot(res)#good


#test
as.data.frame(drop1(res, test="Chisq"))

#for baseline difference

res %>% emmeans(pairwise~ stress|scan) -> emm_res

emm_res %>% 
    summary(by = NULL, adjust = "holm")

```


The 3-fold-interaction is not significant. 



## LMM extracted VBM: Test interaction effect of group:time on UNI-intensity values across all clusters


```{r MODEL-Test-2f-interaction, warning=F, message=F, tidy=T}

#model setup
res <- lmer(value~stress*scan+variable*scan+zlog.MFD+zTIV+(1|ID),data=model.df[complete.cases(model.df),],REML=F)

null <- lmer(value~stress+scan+variable+zlog.MFD+zTIV+(1|ID),model.df[complete.cases(model.df),],REML=F)



#diagnostics

vif(lm(value~scan+stress+variable,model.df))
diagnostics.plot(res)#ok
ranef.diagn.plot(res)#good


#test
anova(null,res, test="Chisq")
as.data.frame(drop1(res, test="Chisq"))

#there is no significant interaction
#drop1 returns F-values instead of CHisq-values-> run full-null-model comparison instead to get Chisq-values for main effect of time

res <- lmer(value~stress+variable+scan+log.MFD+TIV+(1|ID),data=model.df[complete.cases(model.df),],REML=F)

null <- lmer(value~stress+variable+log.MFD+TIV+(1|ID),model.df[complete.cases(model.df),],REML=F)

anova(null,res, test="Chisq")

res <- lmer(value~scan+variable+stress+log.MFD+TIV+(1|ID),data=model.df[complete.cases(model.df),],REML=F)

null <- lmer(value~scan+variable+log.MFD+TIV+(1|ID),model.df[complete.cases(model.df),],REML=F)

anova(null,res, test="Chisq")


#Post-Hoc Tests
##within-group between scans

res %>% emmeans(pairwise~ scan|stress) -> emm_res

emm_res %>% 
    summary(by = NULL, adjust = "holm")

res %>% emmeans(pairwise~ scan|variable) -> emm_res

emm_res %>% 
    summary(by = NULL, adjust = "holm")

res %>% emmeans(pairwise~ stress|scan) -> emm_res

emm_res %>% 
    summary(by = NULL, adjust = "holm")

emm_res_rg <- ref_grid(res,list(pairwise ~ stress|variable))
emm_res1 <- emmeans(regrid(emm_res_rg ),specs=c("scan","stress"))


print(emm_res1)

drop1(null, test="Chisq")


```


There is no significant interaction effect of group and time across all clusters.MFD and TIV have no significant impact on UNI-values. 



A sub-threshold increase in UNI-intensity-values can be observed across all clusters and across both groups. There is additionally a significant main effect of stress, thus stress and control group differ in UNI- values within the VBM-clusters before or after the intervention.



## Plot global change in UNI-intensity values ("raw", no TIV & MFD-correction)

This plot depicts the change in UNI-intensity values across all Clusters from the VBM-Analysis whithout accounting for the influence of TIV and mFD. 

```{r plot, warning=F, message=F, tidy=T,echo=F}

dataraw <- model.df%>%
  dplyr::select(-TIV,-MFD,-log.MFD)

dataraw$variable <- as.factor(gsub(" ","", as.character(dataraw$variable)))

# set labels
dataraw$stress <- factor(dataraw$stress, levels = c("0", "1"))
dataraw$stress <- revalue(dataraw$stress, c("0"="Control", "1"="Stress"))
dataraw$scan <- factor(dataraw$scan, levels = c("pre", "post"))
dataraw$scan <- as.numeric(as.factor(dataraw$scan))


data <- dataraw[is.na(dataraw$variable)==F,]
data <- dataraw %>%
  group_by(stress, scan, ID, variable) %>%
  summarise(value = mean(value)) %>%
  pivot_wider(names_from = c(variable), values_from = value)

data$xj <- jitter(as.numeric(as.factor(data$scan)), amount=0.05)

data_all <- dataraw %>%
  group_by(stress, scan, ID) %>%
  summarise(value = mean(value))

data_all$xj <- jitter(as.numeric(as.factor(data_all$scan)), amount=0.05)
  
summary_data_all <- data_all[complete.cases(data_all),] %>%
  group_by(stress, scan) %>%
  select(-ID) %>%
  summarise_all(funs(mean, std.error))



#Plot 
P_ALL <- ggplot(data = summary_data_all, aes(y = value_mean)) +
  # geoms for raw data
   geom_point(data = data_all %>% filter(scan=="1"), aes(x = xj, colour = stress, y = value), 
              alpha =.6, size=1)+
   geom_point(data = data_all %>% filter(scan=="2"), aes(x = xj, colour = stress, y = value),
              alpha =.6,size=1) +
   geom_half_violin(
     data = data_all %>% filter(scan=="1"),aes(x = scan,fill = stress,y = value), position = position_nudge(x = nudge_cloud_a), 
     side = "l", alpha = .6, color = "black") +
   geom_half_violin(
     data = data_all %>% filter(scan=="2"),aes(x = scan,fill = stress,y = value), position = position_nudge(x = nudge_cloud_b), 
     side = "r", alpha = .6, color = "black") +
   #individual lines
  geom_line(data = data_all, aes(x = xj,y = value,group=ID, colour = stress), position = pd,      alpha = .2)+
  
  # geoms for summary data
  geom_point(data = summary_data_all, aes( x = scan, color = stress, y = value_mean), position = pd, alpha = .6, size = line_summary_size) +
  
 geom_errorbar(data = summary_data_all, aes(x = as.numeric(scan), colour = stress, ymin = value_mean-value_std.error, ymax = value_mean+value_std.error),width = 0, size = line_summary_size, position = pd, alpha = 0.6) +
  
 geom_line(aes(x = scan, color=stress,y = value_mean),  alpha = .6, size =  line_summary_size, position = pd) +
  
  scale_x_continuous(breaks=c(1,2), labels=c("Pre", "Post"), limits=c(0, 3)) +
   
  #Define additional settings
  xlab(NULL) + ylab("UNI intensity across all clusters") +
  #ggtitle('D') +
  theme_cowplot()+
  theme(title = element_text(size=14, face="bold"),axis.text.x = element_text(face = "bold", size = 14),axis.text.y = element_text(face = "bold", size = 14),
        panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(),axis.line = element_line(colour = "black"))+
  scale_fill_manual(values=c(color_1,color_2)) +
  scale_color_manual(values=c(color_1,color_2)) 

print(P_ALL)

##ggsave("plots/UNI_globalplot_violin_raw.jpg",P_ALL)

```


## Plot global intensity change accounted for TIV and MFD



```{r plottivmfd est,echo=F, warning=F, tidy=T, message=F}

res <- lmer(value~stress*scan+variable+zlog.MFD+zTIV+(1|ID),data=model.df[complete.cases(model.df),],REML=F)



plotmodel.emm <- emmeans(res, specs=c("scan","stress"))

plotmodel.conf <- confint(plotmodel.emm)


plotmodel.conf$scan <- relevel(plotmodel.conf$scan, ref = "pre")
plotmodel.conf$scan <- as.numeric(plotmodel.conf$scan)



model.df$scan <- relevel(model.df$scan,ref = "pre")
model.df$stress <- relevel(model.df$stress,ref = "0")

#MOdified ViolinPlot ala Micah Allen
dataraw <- model.df%>%
  dplyr::select(
    ID,
    stress,
    scan,
    variable,
    value)


# set some labels
dataraw$stress <- factor(dataraw$stress, levels = c("0", "1"))
dataraw$scan <- factor(dataraw$scan, levels = c("pre", "post"))
dataraw$scan <- as.numeric(as.factor(dataraw$scan))

#average across clusters(variable)
data_all <- dataraw %>%
  group_by(stress, scan, ID) %>%
  select(-variable) %>%
  dplyr::summarise(value = mean(value))

#add jitter
data_all$xj <- jitter(as.numeric(as.factor(data_all$scan)), amount=0.05)
  


#Plot
P_ALLUNI <- ggplot() +
  # geoms for raw data
   geom_point(data = data_all %>% filter(scan=="1"), aes(x = xj, color = stress, y = value),
               alpha =.4, size=1)+
   geom_point(data = data_all %>% filter(scan=="2"), aes(x = xj, color = stress, y = value),
               alpha =.4,size=1) +
   geom_half_violin(
     data = data_all %>% filter(scan=="1"),aes(x = scan,fill = stress,y = value), position = position_nudge(x = nudge_cloud_a), 
     side = "l", alpha = .6, color = "black") +
   geom_half_violin(
     data = data_all %>% filter(scan=="2"),aes(x = scan,fill = stress,y = value), position = position_nudge(x = nudge_cloud_b), 
     side = "r", alpha = .6, color = "black") +
   #individual lines
  geom_line(data = data_all,aes(x = xj,y = value,group=ID, colour = stress), position = pd, 
      alpha = .2)+
  
  # geoms for summary data
  geom_point(data = plotmodel.conf, aes( x = scan, color = stress, y = emmean), position = pd, alpha = 1, size = line_summary_size) +
  
 geom_errorbar(data = plotmodel.conf, aes(x = scan, colour = stress, ymin = lower.CL, ymax =upper.CL),width = 0, size = line_summary_size, position = pd, alpha = 1) +
  
 geom_line(data = plotmodel.conf, aes(x = scan, color=stress,y = emmean),  alpha = 1, size =  2, position = pd) +
  
  scale_x_continuous(breaks=c(1,2), labels=c("Pre", "Post"), limits=c(0, 3)) +
   
  #Define additional settings
  xlab(NULL) + ylab("UNI intensity") +
  #ggtitle(" 'GMV'-change across all clusters") +
  theme_cowplot()+
  theme(title = element_text(size=11),
      axis.text.x = element_text(size = 9),
      axis.text.y = element_text( size = 11),
axis.title.y = element_text(size = 11),
axis.title.x = element_text( size = 11), 
      panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
        panel.background = element_blank(),axis.line = element_line(colour = "black"))+
  scale_fill_manual(values=c(color_1,color_2), labels = c("Control", "Stress")) +
  scale_color_manual(values=c(color_1,color_2), labels = c("Control", "Stress"))+labs(fill = "Group",color="Group")

print(P_ALLUNI)

ggsave("plots/VBM_UNI_globalplot_violin_est.jpg",P_ALLUNI,width=7,height=5,dpi=300)
ggsave("plots/VBM_UNI_globalplot_violin_est.svg",P_ALLUNI,width=7,height=5,dpi=300)
```



```{r save wsp, warning=F, message=F, tidy=T}
save.image("T1w_analysis.RData")
```

