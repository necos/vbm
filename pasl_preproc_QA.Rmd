---
title: "pASL Preprocessing and QA"
author: "Marie"
date: "8/07/2020"
output: 
  html_document:
        toc: TRUE
        toc_depth: 4
        number_sections: FALSE
        toc_float: TRUE
---

Last Updated: 08/06/2020

# PASL-QA 

This script summarizes the QA of the cerebral blood flow analysis using pasl-data. This data was newly preprocessed by Toralf Mildner in March 2020. The aim of this analysis is to find evidence for changes in cerebral blood-flow within those clusters identified by the VBM-analysis, but also on the whole brain level. 

In this script, 3 different threshold for QA are compared and outliers to be excluded are defined.

## number of non-zero voxels

The preprocessed data includes p-value maps indicating for each voxel the p-value of the statistical comparison between control- and label-image. This comparison should be significant (at least p<0.05, usually p<0.001, but because of low data quality could mean loss of a lot of data).
To optimally trade-off quantity and quality of data, thresholding of p<0.05, p<0.01 and p<0.001 will be compared.

In a first step, a mask was created based on thresholding the images 
/..../pASL/ASL_NECOS_MNI/*...*/EVALUATED/CBF_MNI/*...*.VER1_17_sig_Diff_pVal.nii at p<0.001, p<0.01 and p<0.05 respectively.
Each voxel of this mask indicates, whether meaningful data can be obtained at this location from the cbf map.
Script is: /..../pASL/ASL_NECOS_MNI/scripts/thresh_pvalimages_imcalc_job.m

### Creating mask by thresholding
Matlab-script using Imcalc to threshold maps.
```{asis,echo=F, eval=F, tidy=T}
%-----------------------------------------------------------------------
% Job saved on 27-Feb-2020 15:09:19 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7487)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------

%% Set up basic parameters
clear all


%close all

substring={}

 list_dir = dir('/..../pASL/ASL_NECOS_MNI/*.VER1');
 list_files = '';
 for i = 1:length(list_dir)
       try
      cur_files = dir(['/..../pASL/ASL_NECOS_MNI/' list_dir(i).name '/EVALUATED/CBF_MNI/*_sig_Diff_pVal.nii']);
      %list_files = [list_files,fullfile(list_dir(i).name,{cur_files.name})];
    list_files
    cur_files
    
pvalim=cellstr(spm_select('FPList', cur_files.folder,cur_files.name)); 
pvalim
ending=cur_files.name(1:end-4)
ending
matlabbatch{1}.spm.util.imcalc.input = pvalim;
matlabbatch{1}.spm.util.imcalc.output = sprintf('%s_mask.nii',cur_files.name);
matlabbatch{1}.spm.util.imcalc.outdir = {cur_files.folder};
matlabbatch{1}.spm.util.imcalc.expression = 'i1<0.001';
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = 1;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;

spm_jobman('run', matlabbatch);


    catch ME
    fprintf('ERROR occured for %s\n', cur_files.name);
    continue;  % Jump to next iteration of: for i
  end
  fprintf('Imcalc worked successfully for %s\n',cur_files.name);
    
 end


```

### Binarizing mask and masking cbfmap with this and GM-mask

This thresholded mask was then binarized with FSLmaths and the corresponding cbfmaps were masked with this and the GM mask obtained from a one-sample ttest of the entire sample in SPM. (Same mask used in the VBM of delta-GM images).
from these masked cbfmaps, the amount of non-zero voxels was extracted.
Script: /..../pASL/ASL_NECOS_MNI/scripts/create_pvalmask.sh

This number is used as one indicator of data quality to decide whether the respective image should be included in the analysis (assumption:fewer voxels with meaningful data, worse overall image quality). Additionally, this number averaged across all subjects indicates how much data will be discarded when using the respective threshold.


```{bash, echo=F, eval=F, tidy=T}
#!bin/bash

###Marie Uhlig 26.02.2020


##load FSL environment



for file in /..../pASL/ASL_NECOS_MNI/*.VER1 #selects all folders from newly preprocessed pasl-data



do 


echo $file

scan=${file##*/} #cuts filepath

scan=${scan%%.nii} #cuts ending

echo $scan

nding=$(ls $file/EVALUATED/CBF_MNI/$scan"_"*_sig_Diff_pVal.nii)
nding=${nding}
echo $nding
nding=${nding##*/}
echo $nding
nding=${nding##*.VER1}
ending=${nding%%.nii} 
nending=${nding%%sig_Diff_pVal.nii} 

echo $ending

###pre-images ending="_7_sig_Diff_pVal.nii"
###post-images ending= "_17_sig_Diff_pVal.nii"




###echo "create pvalmask from cbf_pVal (<0.001) to mask cbf.nii"

###fslmaths $file/EVALUATED/CBF_MNI/$scan$ending.nii -uthr 0.001 $file/EVALUATED/CBF_MNI/$scan$ending"_mask.nii.gz" 

###------------------------------>>>>>>>>>>>>>>>>>>>>>>>>>>>><>>>>#for some reason, thresholding results in empty images

###this step was therefore done in matlab, using the script 

###/..../pASL/ASL_NECOS_MNI/scripts/thresh_pvalimages_imcalc_job.m<<<<<<<<<<<<<<<<<<<<<<<<<<<<-------------------------------------

###the resulting mask, $file/EVALUATED/CBF_MNI/$scan$ending"_mask.nii", has however values between 0 and 0.99999999999xxx, therefore, it will be binarized again..

echo "Binarizing pvalmask"

#fslmaths $file/EVALUATED/CBF_MNI/$scan$ending"_mask.nii" -bin $file/EVALUATED/CBF_MNI/$scan$ending"_mask_bin.nii"

echo "Using pvalmask (created with imcalc) mask to mask out all voxels without meaningful data  and Using sampling-specific GMV-mask to mask out all non-gm -voxelsin _cbf.nii"

echo $nending


#fslmaths $file/EVALUATED/CBF_MNI/$scan$nending"cbf.nii.gz" -mul $file/EVALUATED/CBF_MNI/$scan$ending"_mask_bin.nii" -mul /..../CAT12_6_r1450_segment_longitudinal/statistics/ROIs/VBMcluster/2mmNECOSsample_GMmask_fromonesamplettests.nii $file/EVALUATED/CBF_MNI/$scan$nending"cbf_masked.nii.gz"


echo "This is the amount of voxels with meaningful data for this scan"

fslstats $file/EVALUATED/CBF_MNI/$scan$nending"cbf_masked.nii.gz" -V  #gives amount of non-zero voxels

echo $scan >> /..../pASL/ASL_NECOS_MNI/Analysis/"nomeaningfulvoxels".txt

ROIval=$( fslstats $file/EVALUATED/CBF_MNI/$scan$nending"cbf_masked.nii.gz" -V  )

echo $ROIval >> /.../NECOS_structural_18062019/pASL/ASL_NECOS_MNI/Analysis/"nomeaningfulvoxels".txt

done


```


### number of non-zero voxels for p<0.05

```{r visualize p05, echo=F}

nonvoxtab05 <- read.csv("data/nonvoxtab05.csv")


#pre
summary(nonvoxtab05$pre)
hist(nonvoxtab05$pre,main="Number of non-zero voxels per image in pre-scans at p<0.05")
plot(nonvoxtab05$pre,main="Number of non-zero voxels per image in pre-scans at p<0.05")
#post
summary(nonvoxtab05$post)
hist(nonvoxtab05$post,main="Number of non-zero voxels per image in post-scans at p<0.05")
plot(nonvoxtab05$post,main="Number of non-zero voxels per image in post-scans at p<0.05")

```
Minimum:65834 
Mean: 140865
Maximum:164226




The p<0.05 threshold keeps ~70% of voxels within GMmask
(The maskings at p<0.001, p<0.01 and p<0.05 were extensively compared in the pasl_QA_results_markdown) and it was decided to use p<0.05



###Define cutoff for Exclusion


```{r  qa exclusion crit p05, echo=F}

z.nonvoxtab05 <- nonvoxtab05
z.nonvoxtab05$pre <- scale(z.nonvoxtab05$pre)
z.nonvoxtab05$post <- scale(z.nonvoxtab05$post)
z.nonvoxtab05$NECOSID[which.min(z.nonvoxtab05$pre)]
z.nonvoxtab05$NECOSID[z.nonvoxtab05$pre<(-3)]
z.nonvoxtab05$NECOSID[which.min(z.nonvoxtab05$post)]
z.nonvoxtab05$NECOSID[z.nonvoxtab05$post<(-3)]


```

Outliers based on p<0.05 will be used, because in accordance with visual check , more stringent.
check additionally range of values within these masked images to additionally check quality.

```{r outlier exclusion}

out.lier <- c("NECOS036","NECOS049","NECOS066")

length(unique(nonvoxtab05$NECOSID[nonvoxtab05$NECOSID%in%out.lier==F]))

#64 participants will be included in pasl-analysis

nonvoxtab05[nonvoxtab05$NECOSID %in% out.lier==T,colnames(nonvoxtab05)[grepl(".m",colnames(nonvoxtab05))==T]] <- NA



```

Afterwards, images were thresholded at 20 and 100 ml/100mg/min eliminate physiologically meaningless data. This was done in FSL using the scriptsphys_maskout.sh


```{bash physmasking, eval=F}
#!bin/bash

###Marie Uhlig 26.02.2020//update 28.05.2020


##load FSL environment



for file in /..../pASL/ASL_NECOS_MNI/*.VER1 #selects all folders from newly preprocessed pasl-data



do 


echo $file

scan=${file##*/} #cuts filepath

scan=${scan%%.nii} #cuts ending

echo $scan

nding=$(ls $file/EVALUATED/CBF_MNI/$scan"_"*_cbf_masked05.nii)
nding=${nding}
echo $nding
nding=${nding##*/}
echo $nding
nding=${nding##*.VER1}
ending=${nding%%.nii} 
#nending=${nding%%sig_Diff_pVal.nii} 

echo $ending






echo "mask out physiologically meaningless values in cbf_masked05.nii"


fslmaths $file/EVALUATED/CBF_MNI/$scan$ending.nii -thr 20 -uthr 100 /..../pASL/ASL_NECOS_MNI/Analysis/masked_prepoc_cbfimages/$scan$ending"_physmasked.nii.gz" 

#these files were then copied to /..../pASL/ASL_NECOS_MNI/Analysis/masked_prepoc_cbfimages/



done



```

To equalize the distribution of available data across subject images, a sample mask was created. This was done by binarizing the proprocessed and masked out images (results of steps above) and then adding these binarized images together to create a brain mask, indicating for each voxel the amount of available data (minimum:0, maximum: 128, because 64 subjects were included in pasl-analysis). This mask was then thresholded at 100 and binarized. All preprocessed and quality-checked/quality-optimized images were then masked 
with thr result and consequently the same voxels were analysed across all cbf-masks used for the analysis. 

```{bash paslmask, eval=F}
#!bin/bash

###Marie Uhlig 01.04.2020//update 07.08.2020


##load FSL environment


##this script creates a mask indicating the data availability per voxel. Since 64 participants are included in the analysis, 128 is the maximum (2 scans per participant), 0 is the minimum.

#create empty image

fslmaths /..../pASL/ASL_NECOS_MNI/28194.fe_20150902_155201.VER1/EVALUATED/CBF_MNI/28194.fe_20150902_155201.VER1_17_cbf_masked05_physmasked.nii.gz -mul 0 /..../pASL/ASL_NECOS_MNI/Analysis/pasl_samplemask.nii


for file in /..../pASL/ASL_NECOS_MNI/Analysis/masked_prepoc_cbfimages/*_cbf_masked05_physmasked.nii.gz



do 

#exclude outliers

if [[ "$file" == /..../pASL/ASL_NECOS_MNI/Analysis/masked_prepoc_cbfimages/27892.b9*_cbf_masked05_physmasked.nii.gz ]] || [[ "$file" == /..../pASL/ASL_NECOS_MNI/Analysis/masked_prepoc_cbfimages/27969.fd*_cbf_masked05_physmasked.nii.gz ]] || [[ "$file" == /..../pASL/ASL_NECOS_MNI/Analysis/masked_prepoc_cbfimages/28133.5c*_cbf_masked05_physmasked.nii.gz ]] 

then 
        

	echo "This is an outlier and will not be included in the sample-mask"


    else
  

echo $file

scan=${file##*/} #cuts filepath

scan=${scan%%.nii} #cuts ending

echo $scan

#nding=$(ls $file/EVALUATED/CBF_MNI/$scan"_"*_cbf_masked05.nii)
#nding=${nding}
#echo $nding
nding=${file##*/}
#echo $nding
nding=${nding##*.VER1}
scan=${file%%.nii.gz} 
#nending=${nding%%sig_Diff_pVal.nii} 

echo $scan


##binarize images
fslmaths $file -bin $scan"_bin.nii.gz"

echo $scan"_bin.nii.gz"

##add all images to one mask

fslmaths $scan"_bin.nii.gz" -add /..../pASL/ASL_NECOS_MNI/Analysis/pasl_samplemask.nii /..../pASL/ASL_NECOS_MNI/Analysis/pasl_samplemask.nii

      
    fi


done


```

```{bash apply paslmask, eval=F}
#!bin/bash

###Marie Uhlig 02.04.2020//update 28.05.2020


##load FSL environment

#threshold pasl-mask at 100 to mask out all voxels, where less then 100 participants (out of 122 after outlier exclusion) have data and binarize this mask

fslmaths /..../pASL/ASL_NECOS_MNI/Analysis/pasl_samplemask.nii.gz -thr 100 -bin /..../pASL/ASL_NECOS_MNI/Analysis/pasl_samplemask_n100bin.nii.gz 



for file in /..../pASL/ASL_NECOS_MNI/Analysis/masked_prepoc_cbfimages/*_cbf_masked05_physmasked.nii.gz



do 



echo $file

scan=${file##*/} #cuts filepath

scan=${scan%%.nii.gz} #cuts ending

echo $scan




echo "mask out voxels with less then 100 subjects in cbf_masked05_physmasked.nii"

fslmaths $file -mas /..../pASL/ASL_NECOS_MNI/Analysis/masks/pasl_samplemask_n100bin.nii.gz /..../pASL/ASL_NECOS_MNI/Analysis/masked_prepoc_cbfimages/masked_data_availability/$scan"_datamasked.nii.gz"



done



```

```{r, echo=F}
save.image("pasl_2020/pasl_preproc_QA.RData")
```

