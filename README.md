# Rapid brain changes following acute psychosocial stress 

Repository for the analyses reported in the manuscript about voxel based morphometry (VBM) analyses of grey matter volume (GMV) changes after acute psychosocial stress induced by the Trier Social Stress Test (as part of the "Neural Consequences of Stress" study conducted by the [Mind-Body-Emotion group at the Max Planck Institute for Human Cognitive and Brain Sciences](https://www.cbs.mpg.de/departments/neurology/mind-body-emotion)). 

Accompanying paper about time courses of several autonomic, endocrine, and subjective stress measures: [Bae, Reinelt et al., 2019, _PNEC_: Salivary cortisone, as a biomarker for psychosocial stress, is associated with state anxiety and heart rate](https://www.sciencedirect.com/science/article/abs/pii/S030645301830814X)

Accompanying paper about stress-related changes in functional brain connectivity: [Reinelt, Uhlig et al., 2019, _Neuroimage_: Acute psychosocial stress alters thalamic network centrality](https://www.sciencedirect.com/science/article/abs/pii/S1053811919304902)

[Link to a short twitter thread](https://twitter.com/michagaebler/status/1288414264097636352?s=20)

<<<<<<< HEAD

*MODEL SETUP*


**General Info and Structure:**
Linear Mixed Models (LMMs) are used to investigate effects of the TSST vs the Placebo-Intervention over time on imaging markers within the Clusters identified by the VBM analysis.


**Model design**
All Models are compared to a null-model lacking the effect of interest. 
The effect of interest is the interaction of group and time. The two models are compared with a Chi²-Test. 
If the ANOVA comparing those two is significant, the full model explains significantly more variance than the null-model, thus the two groups significantly differ over time in their change in GMV. 

**Fixed Effects**
The full and the null model include the main effect of group and time, no covariates are included in the pasl-analysis. 
The full model addtionally includes the interaction between group and time (effect of interest).

**Random Effects**
A random intercept is modelled per subject. 

**Diagnostics:**
Model assumptions are checked using variance inflation factor (VIF: checks for multicollinearity, should be < 2) and diagnostic plots (e.g., distribution of residuals, functions provided by Dr. Roger Mundry)





=======



